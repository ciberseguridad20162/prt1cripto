/**
 * Created by JUANPABLO on 16/08/2016.
 */

var alphabetic = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

var txtExample = 'WUBEFIQLZURMVOFEHMYMWTIXCGTMPIFKRZUPMVOIRQMMWOZMPULMBNYVQQQMVMVJLEYMHFEFNZPSDLPPSDLPEVQMWCXYMDAVQEEFIQCAYTQOWCXYMWMSEMEFCFWYEYQETRLIQYCGMTWCWFBSMYFPLRXTQYEEXMRULUKSGWFPTLRQAERLUVPMVYQYCXTWFQLMTELSFJPQEHMOZCIWCIWFPZSLMAEZIQVLQMZVPPXAWCSMZMORVGVVQSZETRLQZPBJAXVQIYXEWWOICCGDWHQMMVOWSGNTJPFPPAYBIYBJUTWRLQKLLLMDPYVACDCFQNZPIFPPKSDVPTIDGXMQQVEBMQALKEZMGCVKUZKIZBZLIUAMMVZ';

var planeTxt = $('#txt');
var txtCifrado = $('#txt_cifrado');
var key = $('#key');
var result = '';

$(document).ready(function () {
    txtCifrado.val(txtExample);
});

$('#btn').click(function () {
    result = '';
    var j;
    for (var i = 0; i < planeTxt.val().length; i++) {
        if ((i % key.val().length) == 0) {
            j = 0;
        }
        var index = alphabetic.indexOf(key.val()[j]); // posicion de la key en el array alphabetic
        var indexTxt = alphabetic.indexOf(planeTxt.val()[i]); // posicion de la letra del texto plano en el alphabetic
        if ((index + indexTxt) > 25) {
            result += alphabetic[index + indexTxt - 26];
        } else {
            result += alphabetic[index + indexTxt];
        }
        j++;
    }
    console.log(result);
});

$('#btn_descifrar').click(function () {
    var txtLength = txtCifrado.val();
    var keySize = findEquals(txtLength);
    console.log('El tamaño de la clave es: ' + keySize);

    var proporcion = [];
    var posicion = 0;
    var limit = keySize;
    for (var i = 0; i < txtLength.length; i += keySize) {
        var p = '';
        for (var j = i; j < limit; j++) {
            if (txtLength[j] != undefined)
                p += txtLength[j];
        }
        proporcion[posicion] = p;
        posicion++;
        limit += keySize;
    }
    console.log(proporcion);


    var temporal = [];
    for (var i = 0; i < keySize; i++) {
        var temporaldos = [];
        for (var j = i; j < txtLength.length; j = j + keySize) {
            temporaldos.push(txtLength[j]);
        }
        temporal.push(temporaldos);
    }
    console.log(temporal);

    Array.prototype.unique = function (a) {
        return function () {
            return this.filter(a)
        }
    }(function (a, b, c) {
        return c.indexOf(a, b + 1) < 0
    });

    var otroTemporal = [];
    for (var i = 0; i < temporal.length; i++) {
        otroTemporal.push(temporal[i].unique());
    }
    console.log(otroTemporal);

    var q = [];
    for (var i = 0; i < otroTemporal.length; i++) {
        var equals = [];
        for (var j = 0; j < otroTemporal[i].length; j++) {
            equals[j] = 0;
            for (var k = 0; k < temporal[i].length; k++) {
                if (otroTemporal[i][j] == temporal[i][k]){
                    equals[j] += 1;
                }
            }
        }
        q.push(equals);
    }
    console.log(q);

});

var findEquals = function (text) {
    var range = 4;
    var words = [];
    words.length = 10000;
    var same = [];
    var cont = 0;

    for (var i = 0; i < (text.length - 2); i++) {
        words[i] = "";
        for (var j = i; j < range; j++) {
            words[i] += text[j];
        }
        range++;
    }

    for (var x = 0; x < text.length; x++) {
        for (var y = 0; y < text.length - 2; y++) {
            if (words[x] == words[y] && (x != y)) {
                same.push(Math.abs(y - x));
                cont++;
            }
        }
    }

    Array.prototype.unique = function (a) {
        return function () {
            return this.filter(a)
        }
    }(function (a, b, c) {
        return c.indexOf(a, b + 1) < 0
    });

    var finalArray = [];
    finalArray = same.unique();
    console.log(finalArray);

    var factores = [];
    for (var i = 0; i < finalArray.length; i++) {
        for (var j = 2; j <= 20; j++) {
            if (finalArray[i] % j == 0) {
                factores.push(j);
            }
        }
    }
    console.log(factores);

    var arrayTem = factores.unique();
    var quantity = [];
    console.log(arrayTem);
    for (var i = 0; i < arrayTem.length; i++) {
        quantity[i] = 0;
        for (var j = 0; j < factores.length; j++) {
            if (arrayTem[i] == factores[j]) {
                quantity[i] += 1
            }
        }
    }
    console.log(quantity);

    var max = quantity[0];
    for (var i = 0; i < quantity.length; i++) {
        for (var j = 0; j < quantity.length; j++) {
            if (i != j) {
                if (quantity[j] > max) {
                    max = factores[j];
                }
            }
        }
    }
    return max;
};
