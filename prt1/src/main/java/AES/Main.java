package AES;

import java.util.Scanner;

/**
 * Created by JUANPABLO on 15/08/2016.
 */

public class Main extends Security {

    public static void main(String[] args) {

        Security sec = new Security();


        System.out.println ("Digite 1 para encriptar mensaje");
        System.out.println ("Digite 2 para desencriptar mensaje");
        
        Scanner scanner = new Scanner (System.in); 
        String option = scanner.nextLine (); 
        
        if (option.equals("1")){
            System.out.println ("Por favor, Digite la clave");
            String key = scanner.nextLine ();
            sec.addKey(key);

            System.out.println ("Por favor, digite el mensaje a encriptar. ");
            String text = scanner.nextLine ();
            String crypted = sec.encriptar(text);
            System.out.println("mensaje encriptado: " + crypted );
            
        } else if(option.equals("2")){
            System.out.println ("Por favor, Digite la clave");
            String key = scanner.nextLine ();
            sec.addKey(key);

            System.out.println ("Por favor, digite el mensaje a desencriptar. ");
            String text = scanner.nextLine ();
            String descrypted = sec.desencriptar(text);
            System.out.println("mensaje desencriptado: " + descrypted );
        }
    }
}

