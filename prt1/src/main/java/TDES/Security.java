package TDES;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Created by Santiago on 18/08/2016.
 */
public class Security {

    long time, initTime, finalTime;

    public Security() {
    }

    public String encrypt(String text, String secretKey) throws Exception {
        initTime = System.currentTimeMillis();
        String base64EncryptedString = "";

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] digestOfPassword = messageDigest.digest(secretKey.getBytes("utf-8"));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

        SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] plainTextBytes = text.getBytes("utf-8");
        byte[] buf = cipher.doFinal(plainTextBytes);
        byte[] base64Bytes = Base64.encodeBase64(buf);
        base64EncryptedString = new String(base64Bytes);

        finalTime = System.currentTimeMillis();
        time = finalTime - initTime;
        System.out.println("El tiempo de encripción es: " + time + " ms");
        return base64EncryptedString;
    }

    public String desencrypt(String text, String secretKey) throws Exception {
        initTime = System.currentTimeMillis();
        String base64EncryptedString = "";

        byte[] message = Base64.decodeBase64(text.getBytes("utf-8"));
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, "DESede");

        Cipher decipher = Cipher.getInstance("DESede");
        decipher.init(Cipher.DECRYPT_MODE, key);

        byte[] plainText = decipher.doFinal(message);

        base64EncryptedString = new String(plainText, "UTF-8");

        finalTime = System.currentTimeMillis();
        time = finalTime - initTime;
        System.out.println("El tiempo de encripción es: " + time + " ms");
        return base64EncryptedString;
    }
}
