package TDES;

import java.util.Scanner;

/**
 * Created by Santiago on 18/08/2016.
 */
public class Main {

    public static void main(String[] arg) throws Exception {
        String text = "";
        System.out.println("Digite 1 para encriptar mensaje");
        System.out.println("Digite 2 para desencriptar mensaje");

        Scanner scanner = new Scanner(System.in);
        String option = scanner.nextLine();

        System.out.println("Ingrese la clave: ");
        String key = scanner.nextLine();

        Security security = new Security();

        if (option.equals("1")) {
            System.out.println("Ingrese texto a encriptar: ");
            text = scanner.nextLine();
            System.out.println("Texto encriptado: " + security.encrypt(text, key));
        } else {
            if (option.equals("2")) {
                System.out.println("Ingrese texto a desencriptar: ");
                text = scanner.nextLine();
                System.out.println("Texto desencriptado: " + security.desencrypt(text, key));
            }
        }
    }

}
