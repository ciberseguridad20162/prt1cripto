package DES;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

/**
 * Created by Santiago on 16/08/2016.
 */
public class Security {

    Cipher ecipher;
    Cipher dcipher;

    long time, initTime, finalTime;

    private byte[] salt = {(byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x35,
            (byte) 0xE3, (byte) 0x03};

    public Security(String key) throws Exception {
        int iterationCount = 2;
        initTime = System.currentTimeMillis();
        KeySpec keySpec = new PBEKeySpec(key.toCharArray(), salt, iterationCount);
        SecretKey secretKey = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
        ecipher = Cipher.getInstance(secretKey.getAlgorithm());
        dcipher = Cipher.getInstance(secretKey.getAlgorithm());

        AlgorithmParameterSpec algorithmParameterSpec = new PBEParameterSpec(salt, iterationCount);

        ecipher.init(Cipher.ENCRYPT_MODE, secretKey, algorithmParameterSpec);
        dcipher.init(Cipher.DECRYPT_MODE, secretKey, algorithmParameterSpec);
        finalTime = System.currentTimeMillis();
        time = finalTime - initTime;
    }

    public String encrypt(String text) throws Exception {
        System.out.println("El tiempo de encripción es: " + time + " ms");
        return new BASE64Encoder().encode(ecipher.doFinal(text.getBytes()));
    }

    public String decrypt(String text) throws Exception {
        System.out.println("El tiempo de encripción es: " + time + " ms");
        return new String(dcipher.doFinal(new BASE64Decoder().decodeBuffer(text)));
    }
}
